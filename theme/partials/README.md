# @thumbsup/theme-card-flow

[![NPM](https://img.shields.io/npm/v/@thumbsup/theme-flow.svg?style=flat)](https://www.npmjs.com/package/@thumbsup/theme-flow)
[![Travis CI](https://travis-ci.org/thumbsup/theme-flow.svg?branch=master)](https://travis-ci.org/thumbsup/theme-flow)

# Information for templates

* **content.hbs**
template to render the content (gallery teaser, album and media items)

* **galleryHeader-home.hbs**
template to render the header if no cover item is present.

* **galleryHeader.hbs**
template to render the header if the cover item is present.

* **coverItem.hbs**
the first mediaitem (foto) is used to render the gallery- or album cover teaser.
if the exif ***autor*** properties is set, the cover teaser will be rendered.

```java
    {{!--  cover teaser for the home page   --}}
    {{#compare album.depth '==' 0}}
        {{#compare album.files.length '==' 1}}
            <pre>
                Headline:         {{gallery.title}}
                Autor:            {{album.files.0.meta.exif.Artist}}
                Avatar:           {{album.files.0.meta.exif.Copyright}}
                Description:      {{album.files.0.meta.exif.ImageDescription}}
                Link:             {{album.files.0.meta.exif.DocumentName}}
            </pre>
        {{/compare}}
    {{/compare}}
```

* **albumteaser**
template to render the album teaser for the selected gallery.

* **mediaItems.hbs**
template to render the foto and video items

* **footer.hbs**
template to render the page footer elements.

* **analytics.hbs**
add information for analytics, not used at this moment.
