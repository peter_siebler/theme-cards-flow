/**
 *  Plugin for lightgallery 
 *  Read metadata from a photo and render the foto data layer
 * 
 *  @version 1.0.0
 *  @autor Peter Siebler
 */

(function() {

    var root = this;

    var METADATA = function(obj) {
        if (obj instanceof METADATA) return obj;
        if (!(this instanceof METADATA)) return new METADATA(obj);
        this.METADATAwrapped = obj;
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = METADATA;
        }
        exports.METADATA = METADATA;
    } else {
        root.METADATA = METADATA;
    }

    /**
     * Constructor
     * @param {*} element 
     */
    var Metadata = function(element) {
        // You can access all lightgallery variables and functions like this.
        this.core = $(element).data('lightGallery');
        this.$el = $(element);
        // this.core.s = $.extend({}, defaults, this.core.s)
        this.photodata = [];
        this.html = '';
        this.geodata = null;
        this.fadeDuration = 250;
        this.leafletMap = (typeof L !== 'undefined');
        this.mapIcon = null;
        // get the metakeys translation from the lightgallery settings
        this.metakeys = (typeof this.core.s.metakeys !== 'undefined') ? this.core.s.metakeys : {};
        this.titletext = (typeof this.core.s.metatitle !== 'undefined') ? this.core.s.metatitle : 'Foto Data';
        this.mapheadline = (typeof this.core.s.mapheadline !== 'undefined') ? this.core.s.mapheadline : 'Mapdata';
        this.metafile = null;
        this.init();
        return this;
    }

    /**
     * add the layer for the meta data
     */
    Metadata.prototype.init = function() {
        // Define the base panel template
        if (this.leafletMap) {
            this.mapIcon = L.icon({
                iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAFgUlEQVR4Aa1XA5BjWRTN2oW17d3YaZtr2962HUzbDNpjszW24mRt28p47v7zq/bXZtrp/lWnXr337j3nPCe85NcypgSFdugCpW5YoDAMRaIMqRi6aKq5E3YqDQO3qAwjVWrD8Ncq/RBpykd8oZUb/kaJutow8r1aP9II0WmLKLIsJyv1w/kqw9Ch2MYdB++12Onxee/QMwvf4/Dk/Lfp/i4nxTXtOoQ4pW5Aj7wpici1A9erdAN2OH64x8OSP9j3Ft3b7aWkTg/Fm91siTra0f9on5sQr9INejH6CUUUpavjFNq1B+Oadhxmnfa8RfEmN8VNAsQhPqF55xHkMzz3jSmChWU6f7/XZKNH+9+hBLOHYozuKQPxyMPUKkrX/K0uWnfFaJGS1QPRtZsOPtr3NsW0uyh6NNCOkU3Yz+bXbT3I8G3xE5EXLXtCXbbqwCO9zPQYPRTZ5vIDXD7U+w7rFDEoUUf7ibHIR4y6bLVPXrz8JVZEql13trxwue/uDivd3fkWRbS6/IA2bID4uk0UpF1N8qLlbBlXs4Ee7HLTfV1j54APvODnSfOWBqtKVvjgLKzF5YdEk5ewRkGlK0i33Eofffc7HT56jD7/6U+qH3Cx7SBLNntH5YIPvODnyfIXZYRVDPqgHtLs5ABHD3YzLuespb7t79FY34DjMwrVrcTuwlT55YMPvOBnRrJ4VXTdNnYug5ucHLBjEpt30701A3Ts+HEa73u6dT3FNWwflY86eMHPk+Yu+i6pzUpRrW7SNDg5JHR4KapmM5Wv2E8Tfcb1HoqqHMHU+uWDD7zg54mz5/2BSnizi9T1Dg4QQXLToGNCkb6tb1NU+QAlGr1++eADrzhn/u8Q2YZhQVlZ5+CAOtqfbhmaUCS1ezNFVm2imDbPmPng5wmz+gwh+oHDce0eUtQ6OGDIyR0uUhUsoO3vfDmmgOezH0mZN59x7MBi++WDL1g/eEiU3avlidO671bkLfwbw5XV2P8Pzo0ydy4t2/0eu33xYSOMOD8hTf4CrBtGMSoXfPLchX+J0ruSePw3LZeK0juPJbYzrhkH0io7B3k164hiGvawhOKMLkrQLyVpZg8rHFW7E2uHOL888IBPlNZ1FPzstSJM694fWr6RwpvcJK60+0HCILTBzZLFNdtAzJaohze60T8qBzyh5ZuOg5e7uwQppofEmf2++DYvmySqGBuKaicF1blQjhuHdvCIMvp8whTTfZzI7RldpwtSzL+F1+wkdZ2TBOW2gIF88PBTzD/gpeREAMEbxnJcaJHNHrpzji0gQCS6hdkEeYt9DF/2qPcEC8RM28Hwmr3sdNyht00byAut2k3gufWNtgtOEOFGUwcXWNDbdNbpgBGxEvKkOQsxivJx33iow0Vw5S6SVTrpVq11ysA2Rp7gTfPfktc6zhtXBBC+adRLshf6sG2RfHPZ5EAc4sVZ83yCN00Fk/4kggu40ZTvIEm5g24qtU4KjBrx/BTTH8ifVASAG7gKrnWxJDcU7x8X6Ecczhm3o6YicvsLXWfh3Ch1W0k8x0nXF+0fFxgt4phz8QvypiwCCFKMqXCnqXExjq10beH+UUA7+nG6mdG/Pu0f3LgFcGrl2s0kNNjpmoJ9o4B29CMO8dMT4Q5ox8uitF6fqsrJOr8qnwNbRzv6hSnG5wP+64C7h9lp30hKNtKdWjtdkbuPA19nJ7Tz3zR/ibgARbhb4AlhavcBebmTHcFl2fvYEnW0ox9xMxKBS8btJ+KiEbq9zA4RthQXDhPa0T9TEe69gWupwc6uBUphquXgf+/FrIjweHQS4/pduMe5ERUMHUd9xv8ZR98CxkS4F2n3EUrUZ10EYNw7BWm9x1GiPssi3GgiGRDKWRYZfXlON+dfNbM+GgIwYdwAAAAASUVORK5CYII=',
                iconRetinaUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAABSCAMAAAAhFXfZAAAC91BMVEVMaXEzeak2f7I4g7g3g7cua5gzeKg8hJo3grY4g7c3grU0gLI2frE0daAubJc2gbQwd6QzeKk2gLMtd5sxdKIua5g1frA2f7IydaM0e6w2fq41fK01eqo3grgubJgta5cxdKI1f7AydaQydaMxc6EubJgvbJkwcZ4ubZkwcJwubZgubJcydqUydKIxapgubJctbJcubZcubJcvbJYubJcvbZkubJctbJctbZcubJg2f7AubJcrbZcubJcubJcua5g3grY0fq8ubJcubJdEkdEwhsw6i88vhswuhcsuhMtBjMgthMsrg8srgss6is8qgcs8i9A9iMYtg8spgcoogMo7hcMngMonf8olfso4gr8kfck5iM8jfMk4iM8he8k1fro7itAgesk2hs8eecgzfLcofssdeMg0hc4cd8g2hcsxeLQbdsgZdcgxeLImfcszhM0vda4xgckzhM4xg84wf8Yxgs4udKsvfcQucqhUndROmdM1fK0wcZ8vb5w0eqpQm9MzeKhXoNVcpdYydKNWn9VZotVKltJFjsIwcJ1Rms9OlslLmtH///8+kc9epdYzd6dbo9VHkMM2f7FHmNBClM8ydqVcpNY9hro3gLM9hLczealQmcw3fa46f7A8gLMxc6I3eagyc6FIldJMl9JSnNRSntNNl9JPnNJFi75UnM9ZodVKksg8kM45jc09e6ZHltFBk883gbRBh7pDk9EwcaBzn784g7dKkcY2i81Om9M7j85Llc81is09g7Q4grY/j9A0eqxKmdFFltBEjcXf6fFImdBCiLxJl9FGlNFBi78yiMxVndEvbpo6js74+vx+psPP3+o/ks5HkcpGmNCjwdZCkNDM3ehYoNJEls+lxNkxh8xHks0+jdC1zd5Lg6r+/v/H2ufz9/o3jM3t8/edvdM/k89Th61OiLBSjbZklbaTt9BfptdjmL1AicBHj8hGk9FAgK1dkLNTjLRekrdClc/k7fM0icy0y9tgp9c4jc2NtM9Dlc8zicxeXZn3AAAAQ3RSTlMAHDdTb4yPA+LtnEQmC4L2EmHqB7XA0d0sr478x4/Yd5i1zOfyPkf1sLVq4Nh3FvjxopQ2/STNuFzUwFIwxKaejILpIBEV9wAABhVJREFUeF6s1NdyFEcYBeBeoQIhRAkLlRDGrhIgY3BJL8CVeKzuyXFzzjkn5ZxzzuScg3PO8cKzu70JkO0LfxdTU//pM9vTu7Xgf6KqOVTb9X7toRrVEfBf1HTVjZccrT/2by1VV928Yty9ZbVuucdz90frG8DBjl9pVApbOstvmMuvVgaNXSfAAd6pGxpy6yxf5ph43pS/4f3uoaGm2rdu72S9xzOvMymkZFq/ptDrk90mhW7e4zl7HLzhxGWPR20xmSxJ/VqldG5m9XhaVOA1DadsNh3Pu5L2N6QtPO/32JpqQBVVk20oy/Pi2s23WEvyfHbe1thadVQttvm7Llf65gGmXK67XtupyoM7HQhmXdLS8oGWJNeOJ3C5fG5XCEJnkez3/oFdsvgJ4l2ANZwhrJKk/7OSXa+3Vw2WJMlKnGkobouYk6T0TyX30klOUnTD9HJ5qpckL3EW/w4XF3Xd0FGywXUrstrclVsqz5Pd/sXFYyDnPdrLcQODmGOK47IZb4CmibmMn+MYRzFZ5jg33ZL/EJrWcszHmANy3ARBK/IXtciJy8VsitPSdE3uuHxzougojcUdr8/32atnz/ev3f/K5wtpxUTpcaI45zusVDpYtZi+jg0oU9b3x74h7+n9ABvYEZeKaVq0sh0AtLKsFtqNBdeT0MrSzwwlq9+x6xAO4tgOtSzbCjrNQQiNvQUbUEubvzBUeGw26yDCsRHCoLkTHDa7IdOLIThs/gHvChszh2CimE8peRs47cxANI0lYNB5y1DljpOF0IhzBDPOZnDOqYYbeGKECbPzWnXludPphw5c2YBq5zlwXphIbO4VDCZ0gnPfUO1TwZoYwAs2ExPCedAu9DAjfQUjzITQb3jNj0KG2Sgt6BHaQUdYzWz+XmBktOHwanXjaSTcwwziBcuMOtwBmqPrTOxFQR/DRKKPqyur0aiW6cULYsx6tBm0jXpR/AUWR6HRq9WVW6MRhIq5jLyjbaCTDCijyYJNpCajdyobP/eTw0iexBAKkJ3gA5KcQb2zBXsIBckn+xVv8jkZSaEFHE+jFEleAEfayRU0MouNoBmB/L50Ai/HSLIHxcrpCvnhSQAuakKp2C/YbCylJjXRVy/z3+Kv/RrNcCo+WUzlVEhzKffnTQnxeN9fWF88fiNCUdSTsaufaChKWInHeysygfpIqagoakW+vV20J8uyl6TyNKEZWV4oRSPyCkWpgOLSbkCObT8o2r6tlG58HQquf6O0v50tB7JM7F4EORd2dx/K0w/KHsVkLPaoYrwgP/y7krr3SSMA4zj+OBgmjYkxcdIJQyQRKgg2viX9Hddi9UBb29LrKR7CVVEEEXWojUkXNyfTNDE14W9gbHJNuhjDettN3ZvbOvdOqCD3Jp/9l+/wJE+9PkYGjx/fqkys3S2rMozM/o2106rfMUINo6hVqz+eu/hd1c4xTg0TAfy5kV+4UG6+IthHTU9woWmxuKNbTfuCSfovBCxq7EtHqvYL4Sm6F8GVxsSXHMQ07TOi1DKtZxjWaaIyi4CXWjxPccUw8WVbMYY5wxC1mzEyXMJWkllpRloi+Kkoq69sxBTlElF6aAxYUbjXNlhlDZilDnM4U5SlN5biRsRHnbx3mbeWjEh4mEyiuJDl5XcWVmX5GvNkFgLWZM5qwsop4/AWfLhU1cR7k1VVvcYCWRkOI6Xy5gmnphCYIkvzuNYzHzosq2oNk2RtSs8khfUOfHIDgR6ysYBaMpl4uEgk2U/oJTs9AaTSwma7dT69geAE2ZpEjUsn2ieJNHeKfrI3EcAGJ2ZaNgVuC8EBctCLc57P5u5led6IOBkIYkuQMrmmjChs4VkfOerHqSBkPzZlhe06RslZ3zMjk2sscqKwY0RcjKK+LWbzd7KiHhkncs/siFJ+V5eXxD34B8nVuJEpGJNmxN2gH3vSvp7J70tF+D1Ej8qUJD1TkErAND2GZwTFg/LubvmgiBG3SOvdlsqFQrkEzJCL1rstlnVFROixZoDDSuXQFHESwVGlcuQcMb/b42NgjLowh5MTDFE3vNB5qStRIErdCQEh6pLPR92anSUb/wAIhldAaDMpGgAAAABJRU5ErkJggg==',
                shadowUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAApCAQAAAACach9AAACMUlEQVR4Ae3ShY7jQBAE0Aoz/f9/HTMzhg1zrdKUrJbdx+Kd2nD8VNudfsL/Th///dyQN2TH6f3y/BGpC379rV+S+qqetBOxImNQXL8JCAr2V4iMQXHGNJxeCfZXhSRBcQMfvkOWUdtfzlLgAENmZDcmo2TVmt8OSM2eXxBp3DjHSMFutqS7SbmemzBiR+xpKCNUIRkdkkYxhAkyGoBvyQFEJEefwSmmvBfJuJ6aKqKWnAkvGZOaZXTUgFqYULWNSHUckZuR1HIIimUExutRxwzOLROIG4vKmCKQt364mIlhSyzAf1m9lHZHJZrlAOMMztRRiKimp/rpdJDc9Awry5xTZCte7FHtuS8wJgeYGrex28xNTd086Dik7vUMscQOa8y4DoGtCCSkAKlNwpgNtphjrC6MIHUkR6YWxxs6Sc5xqn222mmCRFzIt8lEdKx+ikCtg91qS2WpwVfBelJCiQJwvzixfI9cxZQWgiSJelKnwBElKYtDOb2MFbhmUigbReQBV0Cg4+qMXSxXSyGUn4UbF8l+7qdSGnTC0XLCmahIgUHLhLOhpVCtw4CzYXvLQWQbJNmxoCsOKAxSgBJno75avolkRw8iIAFcsdc02e9iyCd8tHwmeSSoKTowIgvscSGZUOA7PuCN5b2BX9mQM7S0wYhMNU74zgsPBj3HU7wguAfnxxjFQGBE6pwN+GjME9zHY7zGp8wVxMShYX9NXvEWD3HbwJf4giO4CFIQxXScH1/TM+04kkBiAAAAAElFTkSuQmCC',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                tooltipAnchor: [16, -28],
                shadowSize: [41, 41]
            });
        }
        const template = '<div class="lg-exif hidden">\
                            <div class="lg-exif-topbar">\
                                <h3>' + this.titletext + '</h3>\
                                <span class="lg-hide lg-icon"></span>\
                            </div>\
                            <div class="metadata">\
                               <h3 id="mapsection" class="icon-map">' + this.mapheadline + '</h3>\
                               <div id="map"></div>\
                               <div class="content"></div>\
                            </div> \
                          </div>';

        this.core.$outer.prepend(template);
        this.element = this.core.$outer.find('.lg-exif');
        const _this = this;
        this.$el.on('onAfterSlide.lg', function() {
            _this.update();
            $('.lg-exif .content').fadeIn(_this.fadeDuration);
        });
        this.$el.on('onBeforeSlide.lg', function() {
            $('.lg-exif .content').fadeOut(_this.fadeDuration);
            $('#map').fadeOut(_this.fadeDuration, () => {
                _this.map.setZoom(15);
            });
        });
        const close = this.core.$outer.find('.lg-toolbar .lg-close');
        close.after('<span class="lg-icon lg-info"></span>');
        this.core.$outer.find('.lg-toolbar .lg-info, .lg-exif .lg-hide').on('click', function() {
            _this.element.toggleClass('hidden');
        });
        // Add map to pane, hide until needed
        if (this.leafletMap) {
            this.marker = L.marker([0, 0], { icon: this.mapIcon });
            this.map = L.map('map', {
                zoom: 15,
                scrollWheelZoom: false,
                fadeAnimation: false,
                layers: [
                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                        minZoom: 2
                    }),
                    this.marker
                ],
            });
            $('.icon-map').hide();
            $('#map').hide();
        }
        this.update();
    }

    /**
     * update the meta data view based on the settings
     */
    Metadata.prototype.update = function() {
        const _this = this;
        const thumbnail = this.core.$items.get(this.core.index);
        this.photodata = [];
        // all geo data for the map
        this.geodata = this.getGeo($(thumbnail).data());
        this.metafile = $(thumbnail).data('metafile');
        // try to load meta data from file
        if (this.metafile) {
            $.ajax({
                url: this.metafile,
                dataType: 'json',
                async: false,
                success: function(result) {
                    var m = result[0];
                    if (isEmpty(m) == false) {
                        var b = isEmpty(_this.geodata),
                            h = '',
                            s = '',
                            firstItem = true,
                            _geoDat = {};
                        if (isEmpty(isEmpty) == false) {
                            $.each(m, function(key, item) {
                                if (typeof item === 'object') {
                                    h = decamelize(key, " ");
                                    firstItem = true;
                                    $.each(item, function(key, value) {
                                        if (firstItem)
                                            _this.addPhotodata({ label: h, deep: 0, value: null, key: key });
                                        s = decamelize(key, " ");
                                        _this.addPhotodata({ label: s, deep: 1, value: value, key: key });
                                        firstItem = false;
                                        if (key.startsWith('GPS')) {
                                            _geoDat[key.toLowerCase()] = value;
                                        }
                                    })
                                }
                            });
                        }
                        if (isEmpty(_geoDat) == false) {
                            _this.geodata = _this.getGeo(_geoDat);
                        }
                    }
                }
            });
        }
        // now all others from the photo data-[*] attributes
        var firstItem = true;
        $.each($(thumbnail).data(), function(key, value) {
            if (isEmpty(value) == false) {
                var keytext = _this.getKey(key);
                if (keytext && firstItem && $(thumbnail).data('metasection')) {
                    _this.addPhotodata({ label: $(thumbnail).data('metasection'), deep: 0, value: null, key: "metasection" });
                }
                if (keytext && value) {
                    _this.addPhotodata({ label: keytext, deep: 1, value: value, key: key });
                    firstItem = false;
                }
            }
        });

        if (this.photodata) {
            this.renderData()
        }

    }

    /**
     * add photodata
     * @param string key
     * @param string value
     */
    Metadata.prototype.addPhotodata = function(obj) {
        // { label: s, deep: 0, value: s }
        if (isEmpty(obj)) return;
        if (isEmpty(obj.label) == false) {
            this.photodata.push(obj)
        }
    }

    /**
     * checks if the key is mapped 
     * @param string key
     */
    Metadata.prototype.getKey = function(key) {

        // ignore theese internal used keys -----------------
        if (key == 'src') return null;
        if (key == 'exthumbimage') return null;
        if (key == 'metafile') return null;
        if (key == 'subHtml') return null;
        if (key == 'filename') return null;
        if (key == 'metasection') return null;
        if (key.startsWith('jg.')) return null;
        // ignore theese internal used keys -----------------

        // check meta keys
        if (this.metakeys) {
            if (this.metakeys[key]) {
                return this.metakeys[key];
            }
        }
        return key;
    }

    /**
     * render the data tags from the selected element
     */
    Metadata.prototype.renderData = function() {
        this.html = '';
        var t = this,
            l = '--';
        $.each(this.photodata, function(item) {
            if (this.deep === 0)
                t.html += '<h3 class="icon-caption" id="' + this.key + '">' + this.label + "</h3>"
            if (this.deep === 1 && this.value) {
                if (this.label !== l) {
                    t.html += '<label> - ' + this.label + '</label> <div class="icon-data" id="' + this.key + '"><span>' + this.value + '</span></div>'
                } else {
                    t.html += '<div class="icon-data" id="' + this.key + '"><span>' + this.value + '</span></div>'
                }
            }
            l = this.label
        })
        $('.lg-exif .content').html(this.html);
        if (this.leafletMap) {
            if (this.geodata) {
                $('.icon-map').show();
                $('#map').fadeIn(this.fadeDuration);
                this.map.setView(this.geodata, 13);
                this.marker.setLatLng(this.geodata);
                $('.content').css('height', 'calc(100% - 300px)');
            } else {
                $('.icon-map').hide();
                $('#map').hide();
                $('.content').css('height', 'calc(100% - 10px)');
            }
        }
    }

    /**
     * get the geo coordinates for the map
     * @param (*) geo data
     */
    Metadata.prototype.getGeo = function(data) {
        if (data['gpslatitude'] && data['gpslongitude']) {
            var lat = this.parseDMS(data['gpslatitude']) * ((data['gpslatituderef'] === "S" || data['gpslatituderef'] === "South") ? -1 : 1);
            var lng = this.parseDMS(data['gpslongitude']) * ((data['gpslongituderef'] === "W" || data['gpslongituderef'] === "West") ? -1 : 1);
            return [lat, lng];
        }
        return null;
    }

    /* 
     * Take d/m/s array from exif-js and convert to decimal format 
     */
    Metadata.prototype.parseDMS = function(input) {
        if (input.length === 3) {
            return input[0] + input[1] / 60 + input[2] / 3600;
        }
        // Not in the format we expected
        return input;
    }

    /**
     * Decamelizes a string with/without a custom separator (underscore by default).
     * 
     * @param str String in camelcase
     * @param separator Separator for the new decamelized string.
     */
    function decamelize(str, separator) {
        separator = typeof separator === 'undefined' ? '_' : separator;
        return str
            .replace(/([a-z\d])([A-Z])/g, '$1' + separator + '$2')
            .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1' + separator + '$2')
    }

    /**
     * Camelize a string, cutting the string by multiple separators like
     * hyphens, underscores and spaces.
     * 
     * @param {text} string Text to camelize
     * @return string Camelized text
     */
    function camelize(text) {
        return text.replace(/^([A-Z])|[\s-_]+(\w)/g, function(match, p1, p2, offset) {
            if (p2) return p2.toUpperCase();
            return p1.toLowerCase();
        });
    }

    /**
     * function for dynamic sorting
     * @param {*} key 
     * @param {*} order 
     */
    function compareValues(key, order = 'asc') {
        return function(a, b) {
            if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
                return 0;
            }
            const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key];
            const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key];
            let comparison = 0;
            if (varA > varB) {
                comparison = 1;
            } else if (varA < varB) {
                comparison = -1;
            }
            return (
                (order == 'desc') ? (comparison * -1) : comparison
            );
        };
    }

    /**
     * checks if the value is empty
     * @param {*} value 
     */
    function isEmpty(value) {
        if (value == null || value == undefined) {
            return true;
        }
        if (value.prop && value.prop.constructor === Array) {
            return value.length == 0;
        } else if (typeof value == 'object') {
            return Object.keys(value).length === 0 && value.constructor === Object
        } else if (typeof value == 'string') {
            return value.length == 0;
        } else if (typeof value == 'number') {
            return value == 0;
        } else if (!value) {
            return true;
        }
        return false;
    }

    /**
     * Destroy function must be defined.
     * lightgallery will automatically call your module destroy function 
     * before destroying the gallery
     */
    Metadata.prototype.destroy = function() {
        this.element.remove();
    }

    // Attach your module to lightgallery
    $.fn.lightGallery.modules.metadata = Metadata;


})(jQuery, window, document);