#@thumbsup/theme-card-flow

[![NPM](https://img.shields.io/npm/v/@thumbsup/theme-flow.svg?style=flat)](https://www.npmjs.com/package/@thumbsup/theme-flow)
[![Travis CI](https://travis-ci.org/thumbsup/theme-flow.svg?branch=master)](https://travis-ci.org/thumbsup/theme-flow)

One of the built-in themes for https://github.com/thumbsup/thumbsup.


#Template CARD & FLOWs
This is a template for thumbsup, based on the template card and flow.
For the home and albumpages the card template is used and for the gallery pages the flow template.


![albums](docs/fotobuch.mp4)


**Information**
   Gallery generated successfully!      
   164 albums, 23413 photos, 235 videos  


##Features
- Layout with cards (gallery, album)
- Flow Layout for album fotos 
- Modified thumbsup: add Application settings.
- Album cover with text for gallery- & Album pages
- Cover Text is based on EXIF Data
- Sidemenu to select the albums
- [Tools for METADATA](tools/README.md)
- New lightgallery plugin metadata [How to do see: theme/helpers](theme/helpers/README.md)
- Deploy json files from album folders

---

## Install the theme-card-flow
```bash
  
  ## first create a photoapp folder
     > mkdir ~/photoapp
     > cd ~/photoapp

  ## install the theme 
     > git clone https://bitbucket.org/peter_siebler/theme-cards-flow.git

```

# Install package ´thumbsup´ locally
```bash
  ## install tools 
     > brew install exiftool
     > brew install graphicsmagick
     > brew install ffmpeg
     > brew install gifsicle
     > brew install dcraw
```


## install thumbsup in the current photoapp folder
```bash
     > npm --save install https://github.com/thumbsup/thumbsup.git
```
---

#Requirements
- Install **Thumbsup locally or global**.
- Update the config.json
- Update appsettings.json


* **OPTIONAL**
  - modify the metadata.js [How to do see: theme/helpers](theme/helpers/README.md)
  - add cover images for the albums and galleries
  - add EXIF Data (Artist, Copyright, ImageDescription, DocumentName) to the cover image
  - Update the avatar images (see public/assets/avatar*.*)
  - Update the header backimage (see public/assets/header.jpg)
- run thumbsup to build the fotobook application

---

## Usage

```bash
## run tumbsup locally
   > node_modules/thumbsup/bin/thumbsup.js  --config config.json

## run tumbsup global
   > thumbsup  --config config.json
```

### Configuration sample 
Tip: Always define the paths for all entries **absolute** and not **relative**. 
This ensures that ThumbsUp can always use the correct paths.

```json
## sample for configuration config.json
	{
	    "input": "PATH TO OUTPUT",
	    "output": "./fotobuch",
	    "title": "Fotobuch AUTOR",
	    "include-raw-photos": false,
	    "photo-quality": 90,
	    "thumb-size": 360,
	    "large-size": 1200,
	    "embed-exif": true,
	    "sort-albums-by": "title",
	    "sort-albums-direction": "desc",
	    "theme-path": "PROJECTFOLDER/theme-cards-flow/theme",
	    "theme-settings": "PATH TO PROPERTYFILE.json"
	    "cleanup": true
	}

## sample for theme-settings
  {
	    "author": "AUTORNAME",
	    "logo": "public/assets/avatar.jpg",
	    "application": "tumbsUp Fotobook",
	    "appVersion": "2.1.1",
	    "assets": "public/assets/",
	    "tabHome": "Start",
	    "albumPrefix": "Album",
	    "albumsPrefix": "Alben",
	    "albumCardTitle": "Öffne Album ",
	    "copyright": "2019 ..... All rights reserved.",
	    "navigation": "navi/fbtemp2.json",
	    "footer": "<span>Testlab Fotopalbum</span>",
	    "folders": {
	        "input": "PATH FOTOS",
	        "output": "OUTPUT"
	    },
	    "writeMetaData": {
	        "target": "METADATAFOLDER",
	        "linkfolder": "meta/"
	    }
	}

```
---

## Cover Text & Image
- For this application, the first photo is checked if the EXIF data (Artist, Copyright, ImageDescription, DocumentName) is set there.   
  - If these are present, then the cover teaser for gallery or albums is displayed.
  - If the ImageDescription is empty, no Cover Background Image will be displayed. 
  - if ... a simpe cover w/o a background image will be displayed.
  
- for justified-gallery the first foto will be uses as cover teaser.

### **Folder Structur**
```node
   folder fotos
     cover.img
     + Album 1
       cover.img
       foto1.img
       foto2.img
       + Ablum 1.1
         cover.img
         foto1.img
         foto2.img
     + Ablum 2       
       Album 2.1
         cover.img
         foto1.img
         foto2.img
      Album 2.2
         cover.img
         foto1.img
         foto2.img   
     + Ablum 3       
       foto1.img 
       foto2.img  
```

---

### Helper set exif data for the cover image
```bash
  #!/bin/bash
  # -----------------------------
  # call ./setcover.sh cover.jpg
  # chmod u+x setcover.sh  
  # -----------------------------
  fileName=$1
  echo $fileName

  AUTOR="AUTOR NAME"
  AVATAR="avatar.jpg"
  DESCRIPTION="Lorem Ipsum is simply dummy text of the printing and typesetting industry."
  LINK="URL"

  exiftool -Artist="$AUTOR" -Copyright="$AVATAR" -ImageDescription="$DESCRIPTION"  -DocumentName="$LINK"  $fileName
  
```

---

##Screens

![Start Page](docs/startpage.png)

![Gallery](docs/albumpage.png)

![Foto Page](docs/fotospage.png)

![Side Menu](docs/sideMenu.png)

---

##Developing

Testing the theme

```bash
   > node_modules/thumbsup/bin/thumbsup.js  --input /photos --output ./tmp --theme-path ./theme
```

