#PHOTOS ON MAP Testcase
---

![FOTOS ON MAP](../../docs/testcase_map.png)

## Usage

```bash
## run tumbsup locally and build the gallery
   > node_modules/thumbsup/bin/thumbsup.js  --config config.json

## create the gps data for the map
   > cd theme-cards-flow/tools/gps
   > node index.js
```

##FILES
index.html, testcase with cluster layer

---

**Additional Informations**

***Plugins***
[Leaflet-grayscale](https://github.com/Zverik/leaflet-grayscale)
[Leaflet.markercluster](https://github.com/Leaflet/Leaflet.markercluster)
[Leaflet.Photo](https://github.com/turban/Leaflet.Photo)
[Leaflet.EasyButton](https://github.com/CliffCloud/Leaflet.EasyButton)

---
***MapBBCode Leaflet Plugins Info***
[http://mapbbcode.org/toc.html](http://mapbbcode.org/toc.html)
[http://share.mapbbcode.org](http://share.mapbbcode.org)
[http://mapbbcode.org](http://mapbbcode.org/leaflet.html)





